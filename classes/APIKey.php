<?php

/**
 * Description of APIKey
 *
 * @author patrick
 */
class APIKey {
    
    protected $settings = '';
    
    public function __construct() {
        
        require '../config/config.php'; // get the config file
        include '../config/local_config.php'; // get the local config file if there is one, this should be in gitignore since it contains db usernames and passwords.

        $this->settings = $globalsettings;
        
        return;
    }
    
    public function verifyKey($key, $origin) {

        if(empty($this->settings['domains'][$origin]))
            return 0;
        
        if($this->settings['domains'][$origin] !== $key)
            return 0;
        
        return true;
        
    }
}
