<?php


/**
 * Description of FlightFinder
 * Description: request is parsed, the first variable is the token (not used) 
 *              the second is the apikey to verify if the request is coming from an accepted domain
 *              the third is the verb which corresponds to a function in the FlightFinder class that will process it
 *              the other variables are all arguments that can be passed to the funcitons.
 * Params: $request, the request variables, in this case coming from $_GET
 * Params: $origin, the domain from which the request comes, for varification purposes
 * @author patrick
 */

require_once 'abstract/API.class.php';
require_once 'APIKey.php';
require_once 'User.php';

class FlightFinder extends API
{
    protected $User;

     public function __construct($request, $origin) {
        parent::__construct($request);

        $APIKey = new APIKey();
        $User = new User();
        

        if (!$APIKey->verifyKey($this->key, $origin)) {
            
            throw new Exception('Invalid API Key');
            
        } else if (!$User->get('token', $this->token)) { // not used

            throw new Exception('Invalid User Token'); // not used
        }

        $this->User = $User;
        
    }
     
    
    
    
     
    /**
     * Endpoint: airports
     * Description: Returns all queries related to the listing of airports
     *              for convenience sake, we list only airports in Canada
     */
    
    protected function airports() {
        
        if ($this->method != 'GET') {
            return "Only accepts GET requests";
        }
        
        if($this->verb == 'country' && !empty($this->args[0])) {

            $sql = "SELECT `airport_code`, `city`, `country` FROM `airports` WHERE `country_code` = \"" . $this->args[0] . "\" ORDER BY `airport_code` ASC LIMIT 20";

            return $this->query($sql);
            
        } elseif ($this->verb == 'code' && !empty($this->args[0])) {

            $sql = "SELECT `airport_name` FROM `airports` WHERE `airport_code` = \"" . $this->args[0] . "\" LIMIT 1";
            $return = $this->query($sql);
            return $return[0];
            
        } else {

            $sql = "SELECT `airport_code`, `city`, `country` FROM `airports` ORDER BY `airport_code` ASC LIMIT 20";

            return $this->query($sql);
        }
        
     }
 
     
     
     
     
     
     
    /**
     * Endpoint: flights
     * Description: Returns requests related to flights
     */
     protected function flights() {
         
        if ($this->method != 'GET') {
            return "Only accepts GET requests";
        }
        
        if($this->verb == 'route' && !empty($this->args[0])) {

            $sql = "SELECT * FROM `routes` WHERE `route_id` = \"" . $this->args[0] . "\" LIMIT 1";
            
            return $this->query($sql);
            
        }  elseif ($this->verb == 'trip' && !empty($this->args[0])) {

            $sql = "SELECT  `routes` . *, `trips_routes`.`trips_routes_id` FROM  `routes` ,  `trips_routes` WHERE `trips_routes`.`active` = 1 AND `trips_routes`.`trip` =  '" . $this->args[0]. "' AND  `trips_routes`.`route` =  `routes`.`route_id` ";

            return $this->query($sql);
            
        }   elseif ($this->verb == 'list' && !empty($this->args[0])) {

            $sql = "SELECT * FROM `routes` LIMIT " . $this->args[0];

            return $this->query($sql);
            
        }  elseif ($this->verb == 'assign' && !empty($this->args[0]) && !empty($this->args[1])) {

            $sql = "INSERT INTO `trips_routes` (`trips_routes_id`, `trip`, `route`, `active`, `creation_date`, `last_update_date`) 
                VALUES (NULL, '" . $this->args[0] . "', '" . $this->args[1] . "', '1', '" . date('Y-d-m H:i:s') . "', '" . date('Y-d-m H:i:s') . "');";
            
            $this->query($sql);
            
            return $this->args[1];
            
        } elseif ($this->verb == 'remove' && !empty($this->args[0])) {

            $sql = "UPDATE `trips_routes` SET `active` = '0', `last_update_date` =  '" . date('Y-d-m H:i:s') . "' WHERE `trips_routes_id` = '" . $this->args[0]. "';";
            return $this->query($sql);
            
        } else {

            return "no route selected";
        
        }
        
     }
     
     
     
     
     
    /**
     *  Endpoint: trips
     *  Description: return various queries related to listing, naming trips
     */
     protected function trips() {
         
        if ($this->method != 'GET') {
            return "Only accepts GET requests";
        }
        
        if($this->verb == 'list' && !empty($this->args[0])) {

            $sql = "SELECT * FROM `trips` WHERE `client` = \"" . $this->args[0] . "\" AND `active` = '1'";
            return $this->query($sql);
            
        }
        
        if($this->verb == 'add' && !empty($this->args[0]) && !empty($this->args[1]) && !empty($this->args[2])) {

            $sql = "INSERT INTO `trips` (`trip_id`, `name`, `client`, `active`, `startdate`, `enddate`, `creation_date`, `last_update_date`) 
                    VALUES (null, '" . $this->args[0] . "', '" . $this->args[1] . "', 1, '" . $this->args[2] . "', '" . $this->args[3] . "', '" . date('Y-d-m H:i:s') . "', '" . date('Y-d-m H:i:s') . "')";
            
            return $this->query($sql);
            
        }

        if($this->verb == 'rename' && !empty($this->args[0]) && !empty($this->args[1])) {

            $sql = "UPDATE `trips` SET `name` = '" . $this->args[1] . "', `last_update_date` = '" . date('Y-d-m H:i:s') . "' WHERE `trip_id` = '" . $this->args[0] . "'";
            $this->query($sql);
            return $this->args[0];
            
        }
        
     }
     
     
     
     
     
 }

