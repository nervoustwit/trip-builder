/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$('body').on('click','#list-airports',  function() {
    var content = $("#list-airports-results").html();
    if(content) {
        $("#list-airports-results").html("");
    } else {
    $.getJSON( "http://eyegym.ecmontreal.org/api/v1/38383/jd9fwhfaua9pfpa/airports/country/ca", function( data ) {
      var items = [];
      $.each( data, function( key, val ) {
        items.push( "<li  class='list-group-item' id='" + key + "'>" + val.airport_code + " " + val.city + " " + val.country + "</li>" );
      });

      $( "<ul/>", {
        "class": "list-group",
        html: items.join( "" )
      }).appendTo( "#list-airports-results" );
    });
    }
}).on('click','#list-flights-for-trip',  function() {
    listFlightsForTrip();
}).on('click','#list-flights',  function() {
    var content = $("#list-flights-results").html();

    if(content) {
        $("#list-flights-results").html("");
    } else {

        $.getJSON( "http://eyegym.ecmontreal.org/api/v1/38383/jd9fwhfaua9pfpa/flights/list/10", function( data ) {
          var items = [];
          $.each( data, function( key, val ) {
            items.push( "<li  class='list-group-item' id='flight-" + val.route_id + "'>Airline : " + val.airline_id + " Departure Airport: " + val.source_airport + " Arrival Airport:" + val.destination_airport + " <button class='btn btn-xs pull-right btn-primary add-flight-to-trip' data-flight='" + val.route_id  + "' id='add-flight-" + val.route_id  + "'>Add to Trip 3</button></li>" );
          });

          $( "<ul/>", {
            "class": "list-group",
            html: items.join( "" )
          }).appendTo( "#list-flights-results" );
        });
    }
}).on('click','.add-flight-to-trip',  function() {

    var flight_id = $(this).attr('data-flight');

    $.getJSON( "http://eyegym.ecmontreal.org/api/v1/38383/jd9fwhfaua9pfpa/flights/assign/3/" + flight_id + "", function( data ) {
        $('#add-flight-' + data).addClass('btn-success');
        listFlightsForTrip();
        console.log(data + ' for ' + $(this).attr('id'))
        
    });

}).on('click','.remove-flight-from-trip',  function() {

    var flight_id = $(this).attr('data-flight');
    console.log("http://eyegym.ecmontreal.org/api/v1/38383/jd9fwhfaua9pfpa/flights/remove/" + flight_id + "");
    $.getJSON( "http://eyegym.ecmontreal.org/api/v1/38383/jd9fwhfaua9pfpa/flights/remove/" + flight_id + "", function( data ) {
        
        listFlightsForTrip();
        
    });

}).on('click','#list-trips',  function() {
    var content = $("#list-trips-results").html();

    if(content) {
        $("#list-trips-results").html("");
    } else {

        $.getJSON( "http://eyegym.ecmontreal.org/api/v1/38383/jd9fwhfaua9pfpa/trips/list/38989", function( data ) {
          var items = [];
          $.each( data, function( key, val ) {
            items.push( "<li class='list-group-item form-inline' style='float:left;width:100%' id='trip-" + val.trip_id + "'><input class='form-control col-xs-2' type='text' val='trip-name-" + val.trip_id + "' id='trip-name-" + val.trip_id + "' value=\"" + val.name + "\" /> <button class='btn btn-xs pull-right btn-primary rename-trip' data-trip='" + val.trip_id  + "' id='rename-trip-" + val.trip_id  + "'>Rename</button></li>" );
          });

          $( "<ul/>", {
            "class": "list-group",
            html: items.join( "" )
          }).appendTo( "#list-trips-results" );
        });
    }
}).on('click','.rename-trip',  function() {

    var trip_id = $(this).attr('data-trip');
    var trip_name = $("#trip-name-" + trip_id).val();
    trip_name = encodeURIComponent(trip_name);
    console.log("http://eyegym.ecmontreal.org/api/v1/38383/jd9fwhfaua9pfpa/trips/rename/" + trip_id + "/" + trip_name + "");
    $.getJSON( "http://eyegym.ecmontreal.org/api/v1/38383/jd9fwhfaua9pfpa/trips/rename/" + trip_id + "/" + trip_name + "", function( data ) {
        
        $('#rename-trip-' + data).addClass('btn-success');
        
    });

});

function listFlightsForTrip() {
   $("#list-flights-for-trip-results" ).html("");
    $.getJSON( "http://eyegym.ecmontreal.org/api/v1/38383/jd9fwhfaua9pfpa/flights/trip/3/", function( data ) {
      var items = [];
      $.each( data, function( key, val ) {
        items.push( "<li  class='list-group-item' id='" + key + "'>Airline : " + val.airline_id + " Departure Airport: " + val.source_airport + " Arrival Airport:" + val.destination_airport + " id: " + val.trips_routes_id  + "<button type='button' class='close pull-right remove-flight-from-trip' id='remove-flight-" + val.trips_routes_id  + "' data-flight='" + val.trips_routes_id + "'><span aria-hidden='true'>&times;</span><span class='sr-only'>Remove</span></button></li>" );
      });

      $( "<ul/>", {
        "class": "list-group",
        html: items.join( "" )
      }).appendTo( "#list-flights-for-trip-results" );
    });
}