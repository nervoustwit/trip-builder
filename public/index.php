
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/images/favicon.ico">

    <title>Trip Selector</title>

    <!-- Bootstrap core CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
    
    <!-- specific style -->
    <link rel="stylesheet" href="assets/css/style.css">




    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">
      <div class="header">
        <ul class="nav nav-pills pull-right" role="tablist">
          <li role="presentation" class="active"><a href="#">Home</a></li>
          <li role="presentation"><a href="#">About</a></li>
          <li role="presentation"><a href="#">Contact</a></li>
        </ul>
        <h3 class="text-muted">Trip Selector</h3>
      </div>

      <div class="jumbotron">
        <h1>Trip Selector Exercise</h1>
        <p class="lead">Click on the various links below to get the results.</p>
      </div>
        <p>All the airport and flight data comes from open, downloadable databases. See the code for more comments. To install, copy the files to a linux server, point an apache profile to the /public/ folder. Build the database from the sql file in /data/. Update the config file in /config/ with your db info as well as with the domain/key pair to allow requests from a domian.</p>

      <div class="row marketing">
        <div class="col-lg-12">
          <h4>Get the list of airport</h4>
          <button class="btn btn-md btn-success" id="list-airports">List all airports in Canada</button>
          <p id="list-airports-results"></p>

          <h4>List all the flights for a trip</h4>
          <button class="btn btn-md btn-success" id="list-flights-for-trip">List all flights for trip 3</button>
          <p id="list-flights-for-trip-results"></p>

          <h4>Add a flight for a trip</h4>
          <button class="btn btn-md btn-success" id="list-flights">List all flights</button>
          <p id="list-flights-results"></p>
          
          <h4>Rename a trip</h4>
          <button class="btn btn-md btn-success" id="list-trips">List all trips</button>
          <p id="list-trips-results"></p>

        </div>
      </div>

      <div class="footer">
        <p>&copy; Company 2014</p>
      </div>

    </div> <!-- /container -->


<!-- Latest compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-git2.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="assets/javascript/functions.js"></script>



  </body>
</html>
